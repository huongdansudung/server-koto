import os
class Config(object):
    DEBUG = False
    TESTING = False

    DB_NAME = "VCB_KENSHIN_API_2019"
    DB_USERNAME = "entrytgk"
    DB_PASSWORD = "entrytgk"

    # IMAGE_UPLOADS = "D:\serversgc\Coop_Image_Share\img"

    SQL_DATABASE = "Driver={ODBC Driver 17 for SQL Server};Server=192.168.1.14;Database=" + DB_NAME + "; uid=" + DB_USERNAME + ";pwd=" + DB_PASSWORD + ";MARS_Connection=Yes"
    SESSION_COOKIE_SECURE = True

class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True

    DB_NAME = "VCB_KENSHIN_API_2019"
    DB_USERNAME = "entrytgk"
    DB_PASSWORD = "entrytgk"

    # IMAGE_UPLOADS = "D:\serversgc\Coop_Image_Share\img"

    SQL_DATABASE = "Driver={ODBC Driver 17 for SQL Server};Server=192.168.1.14;Database=" + DB_NAME + "; uid=" + DB_USERNAME + ";pwd=" + DB_PASSWORD + ";MARS_Connection=Yes"
    SESSION_COOKIE_SECURE = True

class TestingConfig(Config):
    TESTING = True

    DB_NAME = "VCB_KENSHIN_API_2019"
    DB_USERNAME = "entrytgk"
    DB_PASSWORD = "entrytgk"

    # IMAGE_UPLOADS = "D:\serversgc\Coop_Image_Share\img"

    SQL_DATABASE = "Driver={ODBC Driver 17 for SQL Server};Server=192.168.1.14;Database=" + DB_NAME + "; uid=" + DB_USERNAME + ";pwd=" + DB_PASSWORD + ";MARS_Connection=Yes"
    SESSION_COOKIE_SECURE = True
