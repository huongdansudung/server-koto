from app import app
from flask import request, jsonify
import pyodbc
import re
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity, create_refresh_token,
    jwt_refresh_token_required
)
from datetime import datetime, timedelta, date
from PIL import Image as imgpil
import base64
import cv2

app.config["ALLOWED_IMAGE_EXTENSIONS"] = ["JPEG", "JPG", "PNG", "GIF"]
app.config['JWT_SECRET_KEY'] = 'super-vbpo'

jwt = JWTManager(app)

def allowed_image(filename):
    # We only want files with a . in the filename
    if not "." in filename:
        return False

    # Split the extension from the filename
    ext = filename.rsplit(".", 1)[1]

    # Check if the extension is in ALLOWED_IMAGE_EXTENSIONS
    if ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False

def connectDB():
    while True:
        try:
            cnn = pyodbc.connect(app.config["SQL_DATABASE"])
            cursor = cnn.cursor()
            return cursor
        except Exception as error:
            print(error)

def crop_image(url,location_):
    img=cv2.imread(url)
    img_crop=img[location_[1]:location_[1]+location_[3],location_[0]:location_[0]+location_[2]]
    retval, buffer = cv2.imencode('.jpg', img_crop)
    image_64_encode = base64.b64encode(buffer)
    return image_64_encode
   
@jwt.expired_token_loader
def my_expired_token_callback(expired_token):
    token_type = expired_token['type']
    if token_type == "refresh":
        return jsonify({
            'status': 402,
            'sub_status': 42,
            'message': 'The {} token has expired'.format(token_type)
        }), 422
    else:
        return jsonify({
            'status': 401,
            'sub_status': 42,
            'message': 'The {} token has expired'.format(token_type)
        }), 401

@app.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        query = "Select ID, Pair, Role, Lvl, LockCtrl, Center, Password, FullName from dbo.AllUser where UserName=N'{username}'".format(username=username)
        try:
            result = connectDB().execute(query).fetchall()
            if result == []:
                data = {'message': 'Auth failed'}
                return jsonify(data), 401
            else:
                if password == result[0][6]:
                    data = {"id": result[0][0], "Pair": result[0][1], "Role": result[0][2], "Lvl": result[0][3], "LockCtrl": result[0][4], "Center": result[0][5],"Password": result[0][6], "Username": username, "FullName": result[0][7]}
                    del data["Password"]
                    access_token = create_access_token(identity=data, expires_delta=timedelta(seconds=100000))
                    refresh_token = create_refresh_token(identity=data, expires_delta=timedelta(seconds=100000))
                    data = {'message': 'Auth successful', 'token': access_token, 'refreshToken': refresh_token}
                    return jsonify(data), 200
                else:
                    data = {'message': 'Auth failed'}
                    return jsonify(data), 401
        except Exception as error:
            data = {'message': error.args}
            return jsonify(data), 500
    data = {"message": "request method is POST"}
    return jsonify(data), 200

@app.route("/changePassword", methods=["POST", "GET"])
def changePassword():
    if request.method == "POST":
        try:
            username = request.form["username"]
            password = request.form["password"]
            newPassword = request.form["newPassword"]

            if username != "" and password != "" and newPassword != "":
                queryUser = "Select ID, Pair, Role, Lvl, LockCtrl, Center, Password from dbo.AllUser where UserName=N'{username}'".format(username=username)
            
                result = connectDB().execute(queryUser).fetchall()
                if result == []:
                    data = {'message': 'Auth failed'}
                    return jsonify(data), 200
                else:
                    if password == result[0][6]:
                        query = "UPDATE dbo.AllUser SET Password = N'{password}' where UPPER(UserName) ='{username}'".format(password=newPassword, username=username)
                        connectDB().execute(query).commit()
                        data = {'message': 'successful'}
                        return jsonify(data), 200
                    else:
                        data = {'message': 'Auth failed'}
                        return jsonify(data), 200
            
            else:
                data = {'message': 'missing params'}
                return jsonify(data), 400
        except Exception as error:
            data = {'message': error.args}
            return jsonify(data), 500
    data = {"message": "request method is POST"}
    return jsonify(data), 200

@app.route("/getImage", methods=["GET"])
@jwt_required
def getImage():
    queryBatch = "SELECT TOP 1 Batch.Id, us1.FormName \
        FROM dbo.[Batch]  left join dbo.[Forms] as us1 on FormId = us1.Id \
        where  HitLv12 = 0 and Batch.Count > 0 and UploadDay >= (select MIN(UploadDay) from dbo.Batch)"

    try:
        resultBatch = connectDB().execute(queryBatch).fetchall()
        if resultBatch == []:
            data = {'message': 'Batch Empty'}
            return jsonify(data), 200
        else:
            queryImage = "Select top 1 id, Url, FieldId \
                From dbo.[{id}--{formName}] \
                Where Lvl = 1 and Content2 is null and datediff(ms, DateP2,GETDATE())>600000".format(id=resultBatch[0][0],formName=resultBatch[0][1])
            resultImage = connectDB().execute(queryImage).fetchall()
            
            # sua test
            queryContent = "Select Top 1 Id from dbo.[{id}--{formName}] where  Lvl = 1 and Content2 is null".format(id=resultBatch[0][0],formName=resultBatch[0][1])
            queryAdd = "Update dbo.Batch set HitLv12= 1 where Id ={id}".format(id=resultBatch[0][0])
            result = connectDB().execute(queryContent).fetchall()
            if result == []:{
                connectDB().execute(queryAdd).commit()
            }
    

            # white batch
            while(resultImage == []):
                reQueryBatch = "SELECT TOP 1 Batch.Id, us1.FormName \
                    FROM dbo.[Batch]  left join dbo.[Forms] as us1 on FormId = us1.Id \
                    where Batch.Id > {resultImageId} and HitLv12 = 0 and Batch.Count > 0 and UploadDay >= (select MIN(UploadDay) from dbo.Batch)".format(resultImageId=resultBatch[0][0])
                resultBatch = connectDB().execute(reQueryBatch).fetchall()
                if resultBatch == []:
                    data = {'message': 'Batch Empty'}
                    return jsonify(data), 200
                
                queryImage = "Select top 1 id, Url, FieldId \
                    From dbo.[{id}--{formName}] \
                    Where Lvl = 1 and Content2 is null and datediff(ms, DateP2,GETDATE())>600000".format(id=resultBatch[0][0],formName=resultBatch[0][1])
                resultImage = connectDB().execute(queryImage).fetchall()

            # update
            queryUpdateImage = "Update dbo.[{id}--{formName}] set DateP2 = GETDATE() where Id={idQueryImage}".format(id=resultBatch[0][0], formName=resultBatch[0][1], idQueryImage=resultImage[0][0])
            connectDB().execute(queryUpdateImage).commit()

            # select location
            queryField = "select Location, FieldName,Scale_Image from dbo.Field where id = {id}".format(id=resultImage[0][2])
            resultField =  connectDB().execute(queryField).fetchall()

            locationCrop  = []
       
            path = r"\\home\vbpo\koto_prj" + resultImage[0][1].split("Image_KOTO")[1]
            
            image = imgpil.open(path.replace("\\","/"))

            print(path.replace("\\","/"))
            print(resultField)

            locationCrop.append(int(float(resultField[0][0].split(",")[0]) * image.width / float(resultField[0][2].split(",")[0])))
            locationCrop.append(int(float(resultField[0][0].split(",")[1]) * image.height / float(resultField[0][2].split(",")[1])))
            locationCrop.append(int(float(resultField[0][0].split(",")[2]) * image.width / float(resultField[0][2].split(",")[0])))
            locationCrop.append(int(float(resultField[0][0].split(",")[3]) * image.height / float(resultField[0][2].split(",")[1])))

            base64_image = crop_image(path.replace("\\","/"), locationCrop)

            data = {'data_image': str(base64_image.decode("utf-8")), 
                'field_name': resultField[0][1],
                'imageId': resultImage[0][0],
                'tableform': "{id}--{formName}".format(id=resultBatch[0][0], formName=resultBatch[0][1]),
                'fieldId': resultImage[0][2]
            }
            return jsonify(data), 200

    except Exception as error:
        data = {'message': error.args}
        return jsonify(data), 500

@app.route("/user", methods=["GET", "POST"])
@jwt_required
def user():
    if request.method == "GET":
        current_user = get_jwt_identity()

        query = "Select UserID, FullName, Total_phieu_nhap, Total_phieu_sai from dbo.[Info_User_Web]  where UserID ={id}".format(id=current_user["id"])
        try:
            result = connectDB().execute(query).fetchall()
            if result == []:
                queryInsertUser = "Insert into dbo.[Info_User_Web] (UserID,FullName,Total_phieu_nhap,Total_phieu_sai) Values ({userid},N'{FullName}',0,0)".format(userid=current_user["id"],FullName=current_user["FullName"])
                connectDB().execute(queryInsertUser).commit()
                data = {'UserID': current_user["id"], 'Fullname': current_user["FullName"], "Total_phieu_nhap": 0, "Total_phieu_sai": 0}
                return jsonify(data), 200
            else:
                data = {'UserID': result[0][0], "Fullname": result[0][1], "Total_phieu_nhap": result[0][2], "Total_phieu_sai": result[0][3]}
                return jsonify(data), 200
        except Exception as error:
            data = {'message': error.args}
            return jsonify(data), 500
    data = {"message": "request method is GET"}
    return jsonify(data), 200    

@app.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    refreshToken = request.form["refreshToken"]
    current_user = get_jwt_identity()
    ret = {
        "message": "Auth successful",
        "token": create_access_token(identity=current_user, expires_delta=timedelta(seconds=100000)),
        "refreshToken": refreshToken
    }
    return jsonify(ret), 200

@app.route('/submit', methods=['POST'])
@jwt_required
def submit():
    content = request.form["content"]
    imageId = request.form["imageId"]
    tableform = request.form["tableform"]
    fieldId = request.form["fieldId"]
    ms = request.form["timeEnter"]
    current_user = get_jwt_identity()

    submit = content + "§§{imageId}§2§{tableform}§{lenContent}§{ms}§{userid1}§0§{fieldid}❤{userid2}❤E1".format(
        imageId=imageId, 
        tableform=tableform,
        lenContent=len(content),
        ms=ms,
        userid1=current_user["id"],
        fieldid=fieldId,
        userid2=current_user["id"]
    )

    try:
     
        strtimetxt = str(datetime.now().strftime('%Y%m%d%H%M%S%f'))
    
        with open(r"//home/vbpo/koto_prj/Sevice/txtP/"+ submit.split('❤')[2]+'-'+submit.split('❤')[1]+'-'+ strtimetxt + '.txt', 'w', encoding='utf-8') as text_file:  #lưu ra file txt
            text_file.write(submit.split('❤')[0])


        #update total enter
        query = "update dbo.[Info_User_Web] set Total_phieu_nhap = (Total_phieu_nhap + 1) where UserID = {userid}".format(userid=current_user["id"])
        connectDB().execute(query).commit()

        data = {'message': 'successful'}
        return jsonify(data), 200

    except Exception as error:
        data = {'message': error.args}
        return jsonify(data), 500


@app.route('/return_image', methods=['POST'])
def returnImage():
    tableform = request.form["tableform"]
    imageId = request.form["imageId"]

    try:
        query = "Update dbo.[" + tableform + "]  set DateP2 = GETDATE() where Id="+imageId+""
        print(query)
        connectDB().execute(query).commit()
        data = {'message': 'successful'}
        return jsonify(data), 200

    except Exception as error:
        data = {'message': error.args}
        return jsonify(data), 500